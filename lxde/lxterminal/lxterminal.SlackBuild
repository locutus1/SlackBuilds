#!/bin/bash

# Slackware build script for lxterminal

# Copyright 2021 All rights reserved.

#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#For the complete terms of the GNU General Public License, and to
#recieve a copy of the GPL please see this URL:
#http://www.gnu.org/licenses/gpl-2.0.html
#----------------------------------------------------------------

cd $(dirname $0) ; CWD=$(pwd)

PKGNAM=lxterminal
VERSION=${VERSION:-0.4.0}
BUILD=${BUILD:-1}
TAG=${TAG:-_gnus}
PKGTYPE=${PKGTYPE:-txz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PKGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
  exit 0
fi

wget --no-check-certificate -c https://sourceforge.net/projects/lxde/files/LXTerminal%20%28terminal%20emulator%29/LXTerminal%200.4.x/lxterminal-0.4.0.tar.xz

TMP=${TMP:-/tmp/build}
PKG=$TMP/package-$PKGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e
trap 'echo "$0 FAILED at line ${LINENO}" | tee $OUTPUT/error-${PKGNAM}.log' ERR

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PKGNAM-$VERSION
tar xvf $CWD/$PKGNAM-$VERSION.tar.xz
cd $PKGNAM-$VERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libexecdir=/usr/libexec \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --mandir=/usr/man \
  --docdir=/usr/doc/$PKGNAM-$VERSION \
  --enable-man \
  --enable-gtk3 \
  --build=$ARCH-slackware-linux 

make
make install-strip DESTDIR=$PKG

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

mkdir -p $PKG/usr/doc/$PKGNAM-$VERSION
cp -a AUTHORS COPYING ChangeLog NEWS \
  $PKG/usr/doc/$PKGNAM-$VERSION
cat $CWD/$PKGNAM.SlackBuild > $PKG/usr/doc/$PKGNAM-$VERSION/$PKGNAM.SlackBuild

# Add a package description:
mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
if [ -f $CWD/doinst.sh ]; then
  cat $CWD/doinst.sh >> $PKG/install/doinst.sh
elif [ -f $CWD/doinst.sh.gz ]; then
  zcat $CWD/doinst.sh.gz >> $PKG/install/doinst.sh
fi
if [ -f $CWD/slack-required ]; then
  cat $CWD/slack-required > $PKG/install/slack-required
fi

# Build the package:
cd $PKG
makepkg --linkadd y --chown n $OUTPUT/${PKGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE} 2>&1 | tee $OUTPUT/makepkg-${PKGNAM}.log
cd $OUTPUT
md5sum ${PKGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE} > ${PKGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE}.md5
cd -
cat $PKG/install/slack-desc | grep "^${PKGNAM}" > $OUTPUT/${PKGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.txt
if [ -f $PKG/install/slack-required ]; then
  cat $PKG/install/slack-required > $OUTPUT/${PKGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.dep
fi

